from spyne import Application, rpc, ServiceBase, Iterable, Integer, Unicode
from spyne.protocol.soap import Soap11
from spyne.server.wsgi import WsgiApplication

#Clase principal, donde se inician los servicios
class RPCService(ServiceBase):
    """docstring fo RPCService."""

    #Servicio saludo
    @rpc(Unicode,_returns=Unicode)
    def say_hello(ctx,name):
        """
        @param name, nombre
        @return Saludo con el nombre indicado
        """
        return u'hello, '+name
    #Servicio suma
    @rpc(Integer, Integer, _returns=Integer)
    def sum(ctx, x, y):
        """
        @param x valor a sumar
        @param y valor a sumar
        @return El resultado de la suma de x con y
        """
        return x+y
    #Servicio lista
    @rpc(Unicode, Integer, _returns = Iterable(Unicode))
    def list_hello(ctx, name, times):
        """
        """
        for i in range(times):
            yield u'Hello, %s' % name


#Se crea un ejemplar de Application indicando los protocolos de entrada y salida
application = Application([RPCService],'spyne.examples.hello.soap',
                            in_protocol=Soap11(validator='lxml'),
                            out_protocol=Soap11())

#Servidor para modo debug
wsgi_application = WsgiApplication(application)

# Parámentros de inicio de la aplicación
if __name__ == '__main__':
    import logging
    from wsgiref.simple_server import make_server

    logging.basicConfig(level=logging.DEBUG)

    server = make_server('127.0.0.1',8000,wsgi_application)
    server.serve_forever()
